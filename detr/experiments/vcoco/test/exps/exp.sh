cd ../../../
exp_id="baseline"

addr="127.0.0.1"
port=1120
num_gpu=4

backbone='resnet50'
dataset='vcoco'
dataset_path='vcoco/'

pretrained='checkpoints/upt/vcoco/upt-r50-vcoco.pth'

export OMP_NUM_THREADS=1
python main.py \
--world-size $num_gpu \
--master_addr $addr \
--master_port $port \
--dataset $dataset \
--data-root $dataset_path \
--partitions trainval test \
--resume $pretrained \
--output-dir logs/$dataset/$exp_id/ 
--cache 
