cd ../../../
exp_id="unary_tokens"

addr="127.0.0.1"
port=1120
num_gpu=4

backbone='resnet50'
dataset='hicodet'
dataset_path='hicodet/'

pretrained='checkpoints/detr/hico/detr-r50-hicodet.pth'


export OMP_NUM_THREADS=1
python main.py \
--world-size $num_gpu \
--master_addr $addr \
--master_port $port \
--dataset $dataset \
--backbone $backbone \
--pretrained $pretrained \
--batch-size 12 \
--output-dir logs/$dataset/$exp_id/ 

python main.py \
--world-size 1 \
--master_addr $addr \
--master_port $port \
--dataset $dataset \
--backbone $backbone \
--batch-size 1 \
--resume logs/$dataset/$exp_id/ckpt_06200_20.pt  \
--output-dir logs/$dataset/$exp_id/ \
--eval
