#!/bin/bash
filename="baseline"

chmod +x "./exps/"$filename".sh"
nohup "./exps/"$filename".sh" >> "./logs/"$filename".txt" 2>&1 & \
echo $! > "./logs/save_pid.txt"