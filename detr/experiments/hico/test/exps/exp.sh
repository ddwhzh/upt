cd ../../../
exp_id="baseline"

addr="127.0.0.1"
port=1120
num_gpu=4

backbone='resnet50'
dataset='hicodet'
dataset_path='hicodet/'

pretrained='checkpoints/detr/hico_det/detr-r50-hicodet.pth'



python main.py \
--world-size $num_gpu \
--master_addr $addr \
--master_port $port \
--dataset $dataset \
--backbone $backbone \
--partitions trainval test \
--resume logs/$dataset/$exp_id/ckpt_06200_20.pt  \
--output-dir logs/$dataset/$exp_id/ 
--eval
